Barebones JastAddJ Build
========================

This project is intended as a simple starting point for making your own
JastAddJ extensions! The project should build without altering the
project configuration.

Use these commands to clone the project and build it the first time:

    git clone --recursive git@bitbucket.org:jastadd/barebones-jj.git
    cd barebones-jj
    gradle jar

Make sure to include the `--recursive` in the clone command to get the JastAddJ
submodule.

Alternatively, you can start from scratch:

    mkdir barebones-jj
    cd barebones-jj
    git init
    git submodule add --depth 1 https://bitbucket.org/jastadd/jastaddj.git jastaddj
    cd jastaddj
    git checkout 9609cd8ec178e8f5c096fe6fc7e391870b63223f
    cd ..
    mkdir -p src/main/resources
    echo "version=Barebones 1.0" > src/main/resources/Version.properties
    git archive --remote=git@bitbucket.org:jastadd/barebones-jj.git HEAD -- build.gradle | tar -x
    gradle jar

If you want to use the latest version of JastAddJ you can skip the checkout
step above.

Requirements
------------

* Git 1.9.1
* Gradle 1.10
* Java 7+

Build Overview
--------------

The Gradle build system is used to build the JastAddJ extension. The build
script `build.gradle` contains the configuration of the build.

A custom Gradle plugin is used to generate Java code from JastAdd modules
The plugin is called JastAddGradle, and is available from the JastAdd maven
repository. The first part of the build script adds the necessary dependency
for the JastAdd plugin:

    buildscript {
        repositories.mavenLocal()
        repositories.maven {
            url 'http://jastadd.org/mvn/'
        }
        dependencies {
            classpath group: 'org.jastadd', name: 'jastaddgradle', version: '1.9'
        }
    }

After that, the plugins to be used are applied:

    apply plugin: 'java'
    apply plugin: 'application'
    apply plugin: 'jastadd'

Then, the JastAdd configuration follows:

    jastadd {
        modules "jastaddj/java4", "jastaddj/java5", "jastaddj/java6", "jastaddj/java7"

        module = "java7 backend"

        astPackage = System.properties['jastadd.packageName'] ?: "AST"
        genDir = "src/gen/java"
        buildInfoDir = "src/gen/resources"
        parser.name = "JavaParser"
    }

The JastAdd configuration has a list of directories where module declarations
can be found. Module declarations are stored in files named `modules`.

The version name is stored in the property file `src/main/resources/Version.properties`


